import type { NextApiRequest, NextApiResponse } from "next";
import {
  generateColorCombinations,
  outputToApiResponse,
} from "../../src/generate";
import { createSunLchLMap } from "../../src/useCases/palette";

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const hex = req.query.hex;
  if (hex == null) {
    res.status(400).json({ error: "クエリ hex が必要です。" });
    return;
  }
  if (Array.isArray(hex)) {
    res.status(400).json({ error: "クエリ hex はひとつだけ指定できます。" });
    return;
  }
  if (!/[0-9a-fA-f]{6}/.test(hex)) {
    res.status(400).json({
      error: "クエリ hex は /[0-9a-fA-f]{6}/ の形式である必要があります。",
    });
  }
  const { output } = generateColorCombinations(
    { original: `#${hex}` },
    createSunLchLMap
  );
  const response = outputToApiResponse(output);
  res.status(200).json(response);
}
