import { HEX } from "color-convert/conversions";
import type { NextPage } from "next";
import Head from "next/head";
import React, { FormEvent, useCallback, useMemo, useState } from "react";
import { BiAnchor, BiHeart, BiMoon, BiStar, BiSun } from "react-icons/bi";
import { ColorCells } from "../src/components/ColorCells";
import { ColorInput } from "../src/components/ColorInput";
import { ColorPicker } from "../src/components/ColorPicker";
import { generateColorCombinations } from "../src/generate";
import {
  createAnchorLchLMap,
  createHeartLchLMap,
  createMoonLchLMap,
  createStarLchLMap,
  createSunLchLMap,
} from "../src/useCases/palette";
import styles from "../styles/Index.module.css";

const Page: NextPage = () => {
  const [hex, setHex] = useState<HEX>("#808080");
  const queryValue = useMemo(() => {
    return hex.replace("#", "");
  }, [hex]);
  const handleInput = useCallback((e: FormEvent<HTMLInputElement>) => {
    if (e.target instanceof HTMLInputElement) {
      setHex(e.target.value);
    }
  }, []);

  return (
    <>
      <Head>
        <title>Color Combinations Generator</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.content}>
          <ColorInput onInput={handleInput} value={hex} />
          <ColorCells
            href={`/api/star?hex=${queryValue}`}
            icon={<BiStar />}
            {...generateColorCombinations({ original: hex }, createStarLchLMap)}
          />
          <ColorCells
            href={`/api/sun?hex=${queryValue}`}
            icon={<BiSun />}
            {...generateColorCombinations({ original: hex }, createSunLchLMap)}
          />
          <ColorCells
            href={`/api/moon?hex=${queryValue}`}
            icon={<BiMoon />}
            {...generateColorCombinations({ original: hex }, createMoonLchLMap)}
          />
          <ColorCells
            href={`/api/anchor?hex=${queryValue}`}
            icon={<BiAnchor />}
            {...generateColorCombinations(
              { original: hex },
              createAnchorLchLMap
            )}
          />
          <ColorCells
            href={`/api/heart?hex=${queryValue}`}
            icon={<BiHeart />}
            {...generateColorCombinations(
              { original: hex },
              createHeartLchLMap
            )}
          />
          <ColorPicker onInput={handleInput} value={hex} />
          <div>
            <a
              href="https://gitlab.com/gyo/color-combinations-generator"
              target="_blank"
              rel="noreferrer"
            >
              README
            </a>
          </div>
        </div>
      </main>
    </>
  );
};

export default Page;
