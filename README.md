# Color Combinations Generator

https://color-combinations-generator.vercel.app/

入力された色からカラーパレットを 5 パターン（Star, Sun, Moon, Anchor, Heart）生成します。

1 つのカラーパレットには、次の表のように 9 種類の色とそれぞれに対応する文字色が含まれます。

|           | 背景色                           | 文字色                            |
| --------- | -------------------------------- | --------------------------------- |
| Light     | 入力色をもとにした非常に明るい色 | 白か黒のいずれか                  |
| Blight    | 入力色をもとにした明るい色       | 白か黒のいずれか                  |
| Default   | 入力色をもとにした通常の色       | 白か黒のいずれか                  |
| Deep      | 入力色をもとにした暗い色         | 白か黒のいずれか                  |
| Dark      | 入力色をもとにした非常に暗い色   | 白か黒のいずれか                  |
| Back      | 白                               | 黒か Light ～ Dark のいずれか     |
| Surface   | 黒                               | 白か Light ～ Dark のいずれか     |
| Error     | オレンジ                         | 白か黒か Light ～ Dark のいずれか |
| Secondary | 入力色をもとにした差し色         | なし                              |

## 背景色の生成仕様

Light ～ Dark と Secondary は、パレットごとに異なるアルゴリズムで生成されます。
詳細は各パターンの説明に記述します。
Back, Surface, Error の色はすべてのパレットで同じです。

## 文字色の生成仕様

文字色は背景色とのコントラストを考慮して決定されます。
コントラストの判定には APCA（Accessible Perceptual Contrast Algorithm）の Lc（Lightness Contrast）を利用します。

Light ～ Dark の文字色は、白か黒のいずれか Lc が高くなる色が選ばれます。
Back, Surface, Error の文字色は、背景色と組み合わせた際に 60 以上の最も低い Lc をとる色が選ばれます。
なお Lc 60 は、APCA において「The minimum level recommended for content text that is not body, column, or block text.」と定義されている値です。

## 差し色の生成仕様

Default の色をもとに、明度と色相を調整して決定されます。

Default の明度が低い場合、差し色はその 1.2 倍程度明るい色になります。
逆に Default の明度が高い場合、差し色はその 1.2 倍程度暗い色になります。

色相は Default の色相を 72 度ずらした色になります。

## Star パレット

L\*C\*h 色空間における明度が 96, 88, 72, 48, 32 であるような 5 色で構成されるパレットです。

彩度は基本的には入力された色の値が使われます。ただし Light, Blight は彩度が低くなるように調整されます。

色相は入力された色の値が使われます。

他のパレットの基準となるようなパレットです。

| 色名 | Light   | Blight  | Default | Deep  | Dark  |
| ---- | ------- | ------- | ------- | ----- | ----- |
| 明度 | 96      | 88      | 72      | 48    | 32    |
| 彩度 | input/5 | input/2 | input   | input | input |
| 色相 | input   | input   | input   | input | input |

## Sun パレット

L\*C\*h 色空間における明度が 96, 92, 82, 66, 55 であるような 5 色で構成されるパレットです。

彩度、色相は Star パレットと同じ仕組みで生成されます。

Star パレットよりも 1.5 倍程度明るいカラーパレットです。

| 色名 | Light   | Blight  | Default | Deep  | Dark  |
| ---- | ------- | ------- | ------- | ----- | ----- |
| 明度 | 96      | 92      | 82      | 66    | 55    |
| 彩度 | input/5 | input/2 | input   | input | input |
| 色相 | input   | input   | input   | input | input |

## Moon パレット

L\*C\*h 色空間における明度が 96, 58, 48, 32, 21 であるような 5 色で構成されるパレットです。

彩度、色相は Star パレットと同じ仕組みで生成されます。

Star パレットよりも 1.5 倍程度暗いカラーパレットです。

| 色名 | Light   | Blight  | Default | Deep  | Dark  |
| ---- | ------- | ------- | ------- | ----- | ----- |
| 明度 | 96      | 58      | 48      | 32    | 21    |
| 彩度 | input/5 | input/2 | input   | input | input |
| 色相 | input   | input   | input   | input | input |

## Anchor パレット

入力された色が必ず中心に配置されるようなパレットです。

彩度、色相は Star パレットと同じ仕組みで生成されます。

| 色名 | Light          | Blight         | Default | Deep           | Dark           |
| ---- | -------------- | -------------- | ------- | -------------- | -------------- |
| 明度 | max(input, 96) | max(input, 88) | input   | min(input, 48) | min(input, 32) |
| 彩度 | input/5        | input/2        | input   | input          | input          |
| 色相 | input          | input          | input   | input          | input          |

## Heart パレット

Star パレットをもとに、入力された色と最も近い色を入力された色に差し替えたパレットです。
ただし Light, Blight は彩度が低くなるように調整されます。

彩度、色相は Star パレットと同じ仕組みで生成されます。

| 色名 | Light       | Blight      | Default     | Deep        | Dark        |
| ---- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 明度 | input \| 96 | input \| 88 | input \| 72 | input \| 48 | input \| 32 |
| 彩度 | input/5     | input/2     | input       | input       | input       |
| 色相 | input       | input       | input       | input       | input       |
