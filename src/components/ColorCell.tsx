import { RGB } from "color-convert/conversions";
import React, { ReactNode, VFC } from "react";
import styles from "../../styles/Index.module.css";
import { rgbToString } from "../useCases/css";

type Props = {
  color?: RGB;
  background: RGB;
  children?: ReactNode;
};

export const ColorCell: VFC<Props> = ({ color, background, children }) => {
  const border = getBorder(background);
  const style = {
    color: color != null ? rgbToString(color) : undefined,
    background: rgbToString(background),
    borderColor: rgbToString(border),
  };
  return (
    <div className={styles.cell} style={style}>
      {children}
    </div>
  );
};

const getBorder = (background: RGB): RGB => {
  const [backgroundR, backgroundG, backgroundB] = background;
  const border =
    backgroundR === 255 && backgroundG === 255 && backgroundB === 255
      ? ([221, 221, 221] as RGB)
      : background;
  return border;
};
