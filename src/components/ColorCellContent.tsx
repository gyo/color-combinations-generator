import { LCH } from "color-convert/conversions";
import React, { VFC } from "react";
import style from "../../styles/Index.module.css";
import { ContrastLc } from "../models/type";

type Props = {
  toneName: string;
  lchL?: LCH[0];
  contrastLc?: ContrastLc;
};

export const ColorCellContent: VFC<Props> = ({
  toneName,
  lchL,
  contrastLc,
}) => {
  const lchLString = lchL != null ? "L*" + lchL : "\u200b";
  const contrastLcString = contrastLc != null ? "Lc" + contrastLc : "\u200b";
  return (
    <>
      <div className={style.colorCellContent__lchL}>{lchLString}</div>
      <div className={style.colorCellContent__name}>{toneName}</div>
      <div className={style.colorCellContent__contrast}>{contrastLcString}</div>
    </>
  );
};
