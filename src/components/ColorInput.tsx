import { HEX } from "color-convert/conversions";
import React, { FormEvent, VFC } from "react";
import styles from "../../styles/Index.module.css";

type Props = {
  value: HEX;
  onInput: (e: FormEvent<HTMLInputElement>) => void;
};

export const ColorInput: VFC<Props> = ({ value, onInput }) => {
  return (
    <input
      className={styles.input}
      value={value}
      onInput={onInput}
      placeholder="#808080"
    />
  );
};
