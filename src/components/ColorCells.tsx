import React, { ReactNode, VFC } from "react";
import styles from "../../styles/Index.module.css";
import {
  BACKGROUND,
  BLACK_TEXT,
  ERROR,
  SURFACE,
  WHITE_TEXT,
} from "../models/const";
import { LchTone, Output } from "../models/type";
import { getContrastLc } from "../useCases/contrast";
import { ColorCell } from "./ColorCell";
import { ColorCellContent } from "./ColorCellContent";

type Props = {
  output: Output;
  ui: LchTone;
  href: string;
  icon: ReactNode;
};

export const ColorCells: VFC<Props> = ({
  output: { tone, text, secondary },
  ui,
  href,
  icon,
}) => {
  const contrasts = getContrasts(tone, text);
  return (
    <div className={styles.colors}>
      <a href={href} target="_blank" rel="noreferrer">
        <ColorCell color={BLACK_TEXT} background={WHITE_TEXT}>
          {icon}
        </ColorCell>
      </a>
      <ColorCell color={text.textOnThemeLight} background={tone.light}>
        <ColorCellContent
          toneName="Light"
          lchL={ui.light[0]}
          contrastLc={contrasts.light}
        />
      </ColorCell>
      <ColorCell color={text.textOnThemeBlight} background={tone.blight}>
        <ColorCellContent
          toneName="Blight"
          lchL={ui.blight[0]}
          contrastLc={contrasts.blight}
        />
      </ColorCell>
      <ColorCell color={text.textOnThemeDefault} background={tone.default}>
        <ColorCellContent
          toneName="Default"
          lchL={ui.default[0]}
          contrastLc={contrasts.default}
        />
      </ColorCell>
      <ColorCell color={text.textOnThemeDeep} background={tone.deep}>
        <ColorCellContent
          toneName="Deep"
          lchL={ui.deep[0]}
          contrastLc={contrasts.deep}
        />
      </ColorCell>
      <ColorCell color={text.textOnThemeDark} background={tone.dark}>
        <ColorCellContent
          toneName="Dark"
          lchL={ui.dark[0]}
          contrastLc={contrasts.dark}
        />
      </ColorCell>
      <ColorCell color={text.themeOnBackground} background={BACKGROUND}>
        <ColorCellContent toneName="Back" contrastLc={contrasts.background} />
      </ColorCell>
      <ColorCell color={text.themeOnSurface} background={SURFACE}>
        <ColorCellContent toneName="Surface" contrastLc={contrasts.surface} />
      </ColorCell>
      <ColorCell color={text.themeOnError} background={ERROR}>
        <ColorCellContent toneName="Error" contrastLc={contrasts.error} />
      </ColorCell>
      <div className={styles.overlapCells}>
        <ColorCell background={tone.default}></ColorCell>
        <ColorCell background={secondary}></ColorCell>
      </div>
    </div>
  );
};

const getContrasts = (tone: Output["tone"], text: Output["text"]) => {
  return {
    light: Math.floor(getContrastLc(text.textOnThemeLight, tone.light)),
    blight: Math.floor(getContrastLc(text.textOnThemeBlight, tone.blight)),
    default: Math.floor(getContrastLc(text.textOnThemeDefault, tone.default)),
    deep: Math.floor(getContrastLc(text.textOnThemeDeep, tone.deep)),
    dark: Math.floor(getContrastLc(text.textOnThemeDark, tone.dark)),
    background: Math.floor(getContrastLc(text.themeOnBackground, BACKGROUND)),
    surface: Math.floor(getContrastLc(text.themeOnSurface, SURFACE)),
    error: Math.floor(getContrastLc(text.themeOnError, ERROR)),
  };
};
