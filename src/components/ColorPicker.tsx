import convert from "color-convert";
import { HEX, RGB } from "color-convert/conversions";
import React, { FormEvent, VFC } from "react";
import styles from "../../styles/Index.module.css";
import { getReadableText } from "../useCases/contrast";
import { rgbToString } from "../useCases/css";

type Props = {
  value: HEX;
  onInput: (e: FormEvent<HTMLInputElement>) => void;
};

export const ColorPicker: VFC<Props> = ({ value, onInput }) => {
  const hexString = getValidHex(value);
  const color = getColorRgb(hexString);
  const style = { color: rgbToString(color), background: hexString };
  const [lchL, lchC, lchH] = convert.hex.lch(hexString);
  return (
    <div className={styles.picker}>
      <div className={styles.picker__inner} style={style}>
        L*C*h = {lchL}, {lchC}, {lchH}
      </div>
      <input
        className={styles.picker__input}
        type="color"
        value={hexString}
        onInput={onInput}
      />
    </div>
  );
};

const getValidHex = (value: HEX): string => {
  const rgb = convert.hex.rgb.raw(value);
  const hex = convert.rgb.hex(rgb);
  const hexString = `#${hex}`;
  return hexString;
};

const getColorRgb = (background: HEX): RGB => {
  const rgb = convert.hex.rgb(background);
  const color = getReadableText(rgb);
  return color;
};
