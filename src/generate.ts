import convert from "color-convert";
import { LCH } from "color-convert/conversions";
import {
  Input,
  LchLTone,
  LchTone,
  Output,
  RgbTextOnTheme,
  RgbThemeOnBackground,
} from "./models/type";
import { convertLchToneToRgbTone } from "./useCases/colorSpace";
import {
  getReadableText,
  pickThemeOnBackground,
  pickThemeOnError,
  pickThemeOnSurface,
} from "./useCases/contrast";
import { rgbToString, rgbToTransparentString } from "./useCases/css";
import { createSecondary } from "./useCases/palette";
import {
  applyBlightLchL,
  applyDarkLchL,
  applyDeepLchL,
  applyDefaultLchL,
  applyLightLchL,
} from "./useCases/tone";

export const generateColorCombinations = (
  input: Input,
  createLchLMap: (original: LCH) => LchLTone
): { output: Output; ui: LchTone } => {
  const originalLch = convert.hex.lch.raw(input.original);
  const targetLchLMap = createLchLMap(originalLch);
  const palette = createPalette(originalLch, targetLchLMap);
  const rgbPalette = convertLchToneToRgbTone(palette);
  const textOnTheme: RgbTextOnTheme = {
    textOnThemeDark: getReadableText(rgbPalette.tone.dark),
    textOnThemeDeep: getReadableText(rgbPalette.tone.deep),
    textOnThemeDefault: getReadableText(rgbPalette.tone.default),
    textOnThemeBlight: getReadableText(rgbPalette.tone.blight),
    textOnThemeLight: getReadableText(rgbPalette.tone.light),
  };
  const themeOnBackground: RgbThemeOnBackground = {
    themeOnBackground: pickThemeOnBackground(rgbPalette.tone),
    themeOnSurface: pickThemeOnSurface(rgbPalette.tone),
    themeOnError: pickThemeOnError(rgbPalette.tone),
  };
  return {
    output: {
      tone: rgbPalette.tone,
      text: {
        ...textOnTheme,
        ...themeOnBackground,
      },
      secondary: rgbPalette.secondary,
    },
    ui: {
      dark: palette.tone.dark.map(Math.round) as LCH,
      deep: palette.tone.deep.map(Math.round) as LCH,
      default: palette.tone.default.map(Math.round) as LCH,
      blight: palette.tone.blight.map(Math.round) as LCH,
      light: palette.tone.light.map(Math.round) as LCH,
    },
  };
};

const createPalette = (
  original: LCH,
  paletteLchLMap: LchLTone
): { tone: LchTone; secondary: LCH } => {
  const defaultLch = applyDefaultLchL(original, paletteLchLMap);
  return {
    tone: {
      dark: applyDarkLchL(original, paletteLchLMap),
      deep: applyDeepLchL(original, paletteLchLMap),
      default: defaultLch,
      blight: applyBlightLchL(original, paletteLchLMap),
      light: applyLightLchL(original, paletteLchLMap),
    },
    secondary: createSecondary(defaultLch),
  };
};

export const outputToApiResponse = (output: Output) => {
  const defaultRgbString = rgbToString(output.tone.default);
  return {
    plain: {
      colors: {
        currentOnSidebarNavigation: rgbToTransparentString(output.tone.blight),
        theme: {
          text: {
            themeOnBackground: rgbToString(output.text.themeOnBackground),
            themeOnSurface: rgbToString(output.text.themeOnSurface),
            themeOnError: rgbToString(output.text.themeOnError),
            textOnThemeDark: rgbToString(output.text.textOnThemeDark),
            textOnThemeDeep: rgbToString(output.text.textOnThemeDeep),
            textOnThemeDefault: rgbToString(output.text.textOnThemeDefault),
            textOnThemeBlight: rgbToString(output.text.textOnThemeBlight),
            textOnThemeLight: rgbToString(output.text.textOnThemeLight),
          },
          tone: {
            dark: rgbToString(output.tone.dark),
            deep: rgbToString(output.tone.deep),
            default: defaultRgbString,
            blight: rgbToString(output.tone.blight),
            light: rgbToString(output.tone.light),
          },
        },
      },
    },
    lakeside: {
      palette: {
        primary: {
          main: defaultRgbString,
        },
        secondary: {
          main: rgbToString(output.secondary),
        },
      },
    },
  };
};
