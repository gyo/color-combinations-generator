import { HEX, LCH, RGB } from "color-convert/conversions";

/**
 * ユーザーから入力される値 HEX
 */
export type Input = {
  original: HEX;
};

/**
 * ユーザーへ返却する値 RGB
 */
export type Output = {
  tone: RgbTone;
  text: RgbTextOnTheme & RgbThemeOnBackground;
  secondary: RGB;
};

/**
 * "tone" としてとりうる値
 */
export type ToneName = "dark" | "deep" | "default" | "blight" | "light";

/**
 * "textOnTheme" としてとりうる値
 */
export type TextOnThemeName =
  | "textOnThemeDark"
  | "textOnThemeDeep"
  | "textOnThemeDefault"
  | "textOnThemeBlight"
  | "textOnThemeLight";

/**
 * "themeOnSomewhere" としてとりうる値
 */
export type ThemeOnBackgroundName =
  | "themeOnBackground"
  | "themeOnSurface"
  | "themeOnError";

export type LchTone = Record<ToneName, LCH>;

export type RgbTone = Record<ToneName, RGB>;

export type RgbTextOnTheme = Record<TextOnThemeName, RGB>;

export type RgbThemeOnBackground = Record<ThemeOnBackgroundName, RGB>;

export type LchL = LCH[0];

export type LchLTone = Record<ToneName, LchL>;

export type ContrastLc = number;

export type Contrasts = [
  { name: "dark"; value: ContrastLc },
  { name: "deep"; value: ContrastLc },
  { name: "default"; value: ContrastLc },
  { name: "blight"; value: ContrastLc },
  { name: "light"; value: ContrastLc }
];

export type Differences = [
  { name: "dark"; value: number },
  { name: "deep"; value: number },
  { name: "default"; value: number },
  { name: "blight"; value: number },
  { name: "light"; value: number }
];
