import { RGB } from "color-convert/conversions";

export const BACKGROUND: RGB = Object.freeze([255, 255, 255]) as RGB;
export const SURFACE: RGB = Object.freeze([51, 51, 51]) as RGB;
export const ERROR: RGB = Object.freeze([241, 73, 36]) as RGB;
export const BLACK_TEXT: RGB = Object.freeze([51, 51, 51]) as RGB;
export const WHITE_TEXT: RGB = Object.freeze([255, 255, 255]) as RGB;

export const LCH_L_MAP_DARK = 32;
export const LCH_L_MAP_DEEP = 48;
export const LCH_L_MAP_DEFAULT = 72;
export const LCH_L_MAP_BLIGHT = 88;
export const LCH_L_MAP_LIGHT = 96;

export const READABLE_CONTRAST_LC = 60;
