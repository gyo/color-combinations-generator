import { generateColorCombinations, outputToApiResponse } from "../generate";
import {
  createAnchorLchLMap,
  createHeartLchLMap,
  createMoonLchLMap,
  createStarLchLMap,
  createSunLchLMap,
} from "../useCases/palette";

describe("generateColorCombinations", () => {
  it("generates star palette", () => {
    expect(
      outputToApiResponse(
        generateColorCombinations({ original: "#2c46c3" }, createStarLchLMap)
          .output
      )
    ).toEqual({
      plain: {
        colors: {
          currentOnSidebarNavigation: "rgba(221, 212, 255, 0.2)",
          theme: {
            text: {
              themeOnBackground: "rgba(89, 99, 230, 1)",
              themeOnSurface: "rgba(221, 212, 255, 1)",
              themeOnError: "rgba(245, 240, 255, 1)",
              textOnThemeDark: "rgba(255, 255, 255, 1)",
              textOnThemeDeep: "rgba(255, 255, 255, 1)",
              textOnThemeDefault: "rgba(51, 51, 51, 1)",
              textOnThemeBlight: "rgba(51, 51, 51, 1)",
              textOnThemeLight: "rgba(51, 51, 51, 1)",
            },
            tone: {
              dark: "rgba(24, 62, 185, 1)",
              deep: "rgba(89, 99, 230, 1)",
              default: "rgba(163, 160, 255, 1)",
              blight: "rgba(221, 212, 255, 1)",
              light: "rgba(245, 240, 255, 1)",
            },
          },
        },
      },
      lakeside: {
        palette: {
          primary: {
            main: "rgba(163, 160, 255, 1)",
          },
          secondary: {
            main: "rgba(0, 165, 233, 1)",
          },
        },
      },
    });
  });

  it("generates sun palette", () => {
    expect(
      outputToApiResponse(
        generateColorCombinations({ original: "#2c46c3" }, createSunLchLMap)
          .output
      )
    ).toEqual({
      plain: {
        colors: {
          currentOnSidebarNavigation: "rgba(233, 224, 255, 0.2)",
          theme: {
            text: {
              themeOnBackground: "rgba(112, 117, 251, 1)",
              themeOnSurface: "rgba(192, 186, 255, 1)",
              themeOnError: "rgba(245, 240, 255, 1)",
              textOnThemeDark: "rgba(255, 255, 255, 1)",
              textOnThemeDeep: "rgba(255, 255, 255, 1)",
              textOnThemeDefault: "rgba(51, 51, 51, 1)",
              textOnThemeBlight: "rgba(51, 51, 51, 1)",
              textOnThemeLight: "rgba(51, 51, 51, 1)",
            },
            tone: {
              dark: "rgba(112, 117, 251, 1)",
              deep: "rgba(144, 144, 255, 1)",
              default: "rgba(192, 186, 255, 1)",
              blight: "rgba(233, 224, 255, 1)",
              light: "rgba(245, 240, 255, 1)",
            },
          },
        },
      },
      lakeside: {
        palette: {
          primary: {
            main: "rgba(192, 186, 255, 1)",
          },
          secondary: {
            main: "rgba(0, 186, 255, 1)",
          },
        },
      },
    });
  });

  it("generates moon palette", () => {
    expect(
      outputToApiResponse(
        generateColorCombinations({ original: "#2c46c3" }, createMoonLchLMap)
          .output
      )
    ).toEqual({
      plain: {
        colors: {
          currentOnSidebarNavigation: "rgba(138, 132, 199, 0.2)",
          theme: {
            text: {
              themeOnBackground: "rgba(138, 132, 199, 1)",
              themeOnSurface: "rgba(245, 240, 255, 1)",
              themeOnError: "rgba(245, 240, 255, 1)",
              textOnThemeDark: "rgba(255, 255, 255, 1)",
              textOnThemeDeep: "rgba(255, 255, 255, 1)",
              textOnThemeDefault: "rgba(255, 255, 255, 1)",
              textOnThemeBlight: "rgba(255, 255, 255, 1)",
              textOnThemeLight: "rgba(51, 51, 51, 1)",
            },
            tone: {
              dark: "rgba(0, 38, 155, 1)",
              deep: "rgba(22, 61, 184, 1)",
              default: "rgba(88, 98, 229, 1)",
              blight: "rgba(138, 132, 199, 1)",
              light: "rgba(245, 240, 255, 1)",
            },
          },
        },
      },
      lakeside: {
        palette: {
          primary: {
            main: "rgba(88, 98, 229, 1)",
          },
          secondary: {
            main: "rgba(0, 166, 234, 1)",
          },
        },
      },
    });
  });

  it("generates anchor palette", () => {
    expect(
      outputToApiResponse(
        generateColorCombinations({ original: "#2c46c3" }, createAnchorLchLMap)
          .output
      )
    ).toEqual({
      plain: {
        colors: {
          currentOnSidebarNavigation: "rgba(221, 212, 255, 0.2)",
          theme: {
            text: {
              themeOnBackground: "rgba(44, 70, 195, 1)",
              themeOnSurface: "rgba(221, 212, 255, 1)",
              themeOnError: "rgba(245, 240, 255, 1)",
              textOnThemeDark: "rgba(255, 255, 255, 1)",
              textOnThemeDeep: "rgba(255, 255, 255, 1)",
              textOnThemeDefault: "rgba(255, 255, 255, 1)",
              textOnThemeBlight: "rgba(51, 51, 51, 1)",
              textOnThemeLight: "rgba(51, 51, 51, 1)",
            },
            tone: {
              dark: "rgba(24, 62, 185, 1)",
              deep: "rgba(44, 70, 195, 1)",
              default: "rgba(44, 70, 195, 1)",
              blight: "rgba(221, 212, 255, 1)",
              light: "rgba(245, 240, 255, 1)",
            },
          },
        },
      },
      lakeside: {
        palette: {
          primary: {
            main: "rgba(44, 70, 195, 1)",
          },
          secondary: {
            main: "rgba(0, 141, 207, 1)",
          },
        },
      },
    });
  });

  it("generates heart palette", () => {
    expect(
      outputToApiResponse(
        generateColorCombinations({ original: "#2c46c3" }, createHeartLchLMap)
          .output
      )
    ).toEqual({
      plain: {
        colors: {
          currentOnSidebarNavigation: "rgba(221, 212, 255, 0.2)",
          theme: {
            text: {
              themeOnBackground: "rgba(89, 99, 230, 1)",
              themeOnSurface: "rgba(221, 212, 255, 1)",
              themeOnError: "rgba(245, 240, 255, 1)",
              textOnThemeDark: "rgba(255, 255, 255, 1)",
              textOnThemeDeep: "rgba(255, 255, 255, 1)",
              textOnThemeDefault: "rgba(51, 51, 51, 1)",
              textOnThemeBlight: "rgba(51, 51, 51, 1)",
              textOnThemeLight: "rgba(51, 51, 51, 1)",
            },
            tone: {
              dark: "rgba(44, 70, 195, 1)",
              deep: "rgba(89, 99, 230, 1)",
              default: "rgba(163, 160, 255, 1)",
              blight: "rgba(221, 212, 255, 1)",
              light: "rgba(245, 240, 255, 1)",
            },
          },
        },
      },
      lakeside: {
        palette: {
          primary: {
            main: "rgba(163, 160, 255, 1)",
          },
          secondary: {
            main: "rgba(0, 165, 233, 1)",
          },
        },
      },
    });
  });
});
