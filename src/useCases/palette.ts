import { LCH } from "color-convert/conversions";
import {
  LCH_L_MAP_BLIGHT,
  LCH_L_MAP_DARK,
  LCH_L_MAP_DEEP,
  LCH_L_MAP_DEFAULT,
  LCH_L_MAP_LIGHT,
} from "../models/const";
import { Differences, LchLTone } from "../models/type";

export const createStarLchLMap = (): LchLTone => {
  return {
    dark: LCH_L_MAP_DARK,
    deep: LCH_L_MAP_DEEP,
    default: LCH_L_MAP_DEFAULT,
    blight: LCH_L_MAP_BLIGHT,
    light: LCH_L_MAP_LIGHT,
  };
};

export const createMoonLchLMap = (): LchLTone => {
  return {
    dark: LCH_L_MAP_DARK * 0.66,
    deep: LCH_L_MAP_DEEP * 0.66,
    default: LCH_L_MAP_DEFAULT * 0.66,
    blight: LCH_L_MAP_BLIGHT * 0.66,
    light: LCH_L_MAP_LIGHT,
  };
};

export const createSunLchLMap = (): LchLTone => {
  return {
    dark: 100 - (100 - LCH_L_MAP_DARK) * 0.66,
    deep: 100 - (100 - LCH_L_MAP_DEEP) * 0.66,
    default: 100 - (100 - LCH_L_MAP_DEFAULT) * 0.66,
    blight: 100 - (100 - LCH_L_MAP_BLIGHT) * 0.66,
    light: LCH_L_MAP_LIGHT,
  };
};

export const createAnchorLchLMap = (original: LCH): LchLTone => {
  const [originalLchL, ,] = original;
  return {
    dark: Math.min(originalLchL, LCH_L_MAP_DARK),
    deep: Math.min(originalLchL, LCH_L_MAP_DEEP),
    default: originalLchL,
    blight: Math.max(originalLchL, LCH_L_MAP_BLIGHT),
    light: Math.max(originalLchL, LCH_L_MAP_LIGHT),
  };
};

export const createHeartLchLMap = (original: LCH): LchLTone => {
  const [originalLchL, ,] = original;
  const differences: Differences = [
    { name: "dark", value: Math.abs(LCH_L_MAP_DARK - originalLchL) },
    { name: "deep", value: Math.abs(LCH_L_MAP_DEEP - originalLchL) },
    { name: "default", value: Math.abs(LCH_L_MAP_DEFAULT - originalLchL) },
    { name: "blight", value: Math.abs(LCH_L_MAP_BLIGHT - originalLchL) },
    { name: "light", value: Math.abs(LCH_L_MAP_LIGHT - originalLchL) },
  ];
  const minDifferences = differences.reduce((previousValue, currentValue) => {
    if (previousValue.value <= currentValue.value) {
      return previousValue;
    } else {
      return currentValue;
    }
  });
  return {
    dark: LCH_L_MAP_DARK,
    deep: LCH_L_MAP_DEEP,
    default: LCH_L_MAP_DEFAULT,
    blight: LCH_L_MAP_BLIGHT,
    light: LCH_L_MAP_LIGHT,
    [minDifferences.name]: originalLchL,
  };
};

export const createSecondary = (defaultLch: LCH): LCH => {
  const [lchL, lchC, lchH] = defaultLch;
  return [
    lchL < 50 ? 100 - (100 - lchL) * 0.8 : lchL * 0.8,
    lchC,
    (lchH - 72) % 360,
  ];
};
