import { LCH } from "color-convert/conversions";
import { LchLTone, ToneName } from "../models/type";

const applyLchL = (lch: LCH, lchLTone: LchLTone, toneName: ToneName): LCH => {
  const [, lchC, lchH] = lch;
  const lchL = lchLTone[toneName];
  return [lchL, lchC, lchH];
};

export const applyDarkLchL = (original: LCH, lchLTone: LchLTone): LCH => {
  const applied = applyLchL(original, lchLTone, "dark");
  return applied;
};

export const applyDeepLchL = (original: LCH, lchLTone: LchLTone): LCH => {
  const applied = applyLchL(original, lchLTone, "deep");
  return applied;
};

export const applyDefaultLchL = (original: LCH, lchLTone: LchLTone): LCH => {
  const applied = applyLchL(original, lchLTone, "default");
  return applied;
};

export const applyBlightLchL = (original: LCH, lchLTone: LchLTone): LCH => {
  const applied = applyLchL(original, lchLTone, "blight");
  const [appliedL, appliedC, appliedH] = applied;
  return [appliedL, appliedC / 2, appliedH];
};

export const applyLightLchL = (original: LCH, lchLTone: LchLTone): LCH => {
  const applied = applyLchL(original, lchLTone, "light");
  const [appliedL, appliedC, appliedH] = applied;
  return [appliedL, appliedC / 5, appliedH];
};
