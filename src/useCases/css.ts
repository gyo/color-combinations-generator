import { RGB } from "color-convert/conversions";

export const rgbToString = (rgb: RGB) => {
  return `rgba(${Math.round(rgb[0])}, ${Math.round(rgb[1])}, ${Math.round(
    rgb[2]
  )}, 1)`;
};

export const rgbToTransparentString = (rgb: RGB) => {
  return `rgba(${Math.round(rgb[0])}, ${Math.round(rgb[1])}, ${Math.round(
    rgb[2]
  )}, 0.2)`;
};
