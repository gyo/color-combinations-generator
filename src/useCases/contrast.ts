import { APCAcontrast, sRGBtoY } from "apca-w3";
import { RGB } from "color-convert/conversions";
import {
  BACKGROUND,
  BLACK_TEXT,
  ERROR,
  READABLE_CONTRAST_LC,
  SURFACE,
  WHITE_TEXT,
} from "../models/const";
import { ContrastLc, Contrasts, RgbTone } from "../models/type";

export const getContrastLc = (text: RGB, background: RGB): ContrastLc => {
  const contrastLc = APCAcontrast(sRGBtoY(text), sRGBtoY(background));
  if (typeof contrastLc !== "number") {
    throw new Error("😱😱😱😱😱");
  }
  return Math.abs(contrastLc);
};

export const getReadableText = (background: RGB): RGB => {
  const contrastLcWithBlackText = getContrastLc(BLACK_TEXT, background);
  const contrastLcWithWhiteText = getContrastLc(WHITE_TEXT, background);
  if (contrastLcWithBlackText >= contrastLcWithWhiteText) {
    return BLACK_TEXT;
  } else {
    return WHITE_TEXT;
  }
};

const getContrasts = (tone: RgbTone, background: RGB): Contrasts => {
  return [
    { name: "dark", value: getContrastLc(tone.dark, background) },
    { name: "deep", value: getContrastLc(tone.deep, background) },
    { name: "default", value: getContrastLc(tone.default, background) },
    { name: "blight", value: getContrastLc(tone.blight, background) },
    { name: "light", value: getContrastLc(tone.light, background) },
  ];
};

const pickMinimumReadableText = (tone: RgbTone, background: RGB) => {
  const contrasts = getContrasts(tone, background);

  // コントラストの低い順に並べる
  contrasts.sort((a, b) => {
    if (a.value < b.value) {
      return -1;
    } else {
      return 1;
    }
  });

  // 最低限のコントラストを確保できる tone を取得する
  const minimumReadableContrast = contrasts.find((contrast) => {
    return contrast.value >= READABLE_CONTRAST_LC;
  });

  if (minimumReadableContrast != null) {
    return tone[minimumReadableContrast.name];
  } else {
    return getReadableText(background);
  }
};

export const pickThemeOnBackground = (tone: RgbTone): RGB => {
  return pickMinimumReadableText(tone, BACKGROUND);
};

export const pickThemeOnSurface = (tone: RgbTone): RGB => {
  return pickMinimumReadableText(tone, SURFACE);
};

export const pickThemeOnError = (tone: RgbTone): RGB => {
  return pickMinimumReadableText(tone, ERROR);
};
