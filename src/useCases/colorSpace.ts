import convert from "color-convert";
import { LCH, RGB } from "color-convert/conversions";
import { LchTone, RgbTone } from "../models/type";

export const convertLchToneToRgbTone = ({
  tone,
  secondary,
}: {
  tone: LchTone;
  secondary: LCH;
}): {
  tone: RgbTone;
  secondary: RGB;
} => {
  return {
    tone: {
      dark: convert.lch.rgb.raw(tone.dark),
      deep: convert.lch.rgb.raw(tone.deep),
      default: convert.lch.rgb.raw(tone.default),
      blight: convert.lch.rgb.raw(tone.blight),
      light: convert.lch.rgb.raw(tone.light),
    },
    secondary: convert.lch.rgb.raw(secondary),
  };
};
